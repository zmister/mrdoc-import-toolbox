# 觅思文档导入工具箱

#### 介绍
MrDoc 觅思文档的文档导入工具箱，支持本地 Markdown 文件、本地 HTML 文件、语雀知识库、印象笔记、Joplin等

详细使用说明见：https://doc.mrdoc.pro/doc/44654/

#### 软件架构
脚本使用`Python`进行编写，并提供基于`PyInstaller`打包的`exe`可执行文件程序。

#### 支持类型

**1. 觅思文档导出的文集**

> 同时支持通用的 Markdown 文件夹导入

脚本名：mrdoc2mrdoc.py

程序名：mrdoc2mrdoc.exe

**2. 语雀知识库**

脚本名：yuque2mrdoc.py

程序名：yuque2mrdoc.exe

**3. 印象笔记**

脚本名：evernote2mrdoc.py

程序名：evernote2mrdoc.exe

#### 配置文件

本工具均基于命令行终端进行交互，你可以在程序同级目录下创建名为`config.ini`的配置文件，以略过在终端中输入相关信息。

```
[mrdoc]
url = 你的觅思文档地址
token = 你的觅思文档用户token
dir = 待导入的文集文件夹

[yuque]
url = 可配置为自己的语雀空间域名，默认不需要配置
token = 语雀用户token
```
